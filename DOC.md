# Dokumentáció a kész alkalmazáshoz

## Kliensoldali szolgáltatások listája, rövid leírással
- Bejelentkezés
	Az alkalmazást alapvetően regisztrált felhasználók használhatják. Az alkalmazás megnyitásakor a bejelentkező oldalon be kell jelentkezni ahhoz, hogy a további felületek hozzáférhetővé váljanak a felhasználó számára. Ezek a jogkörök függnek a felhasználó által betöltött szerepkörtől (pl. diák, tanár...)
- Tantárgyak, jegyek megtekintése:
	A diákok meg tudják tekinteni a hozzájuk kapcsolódó tárgyakat, valamint az osztályzataikat, amelyeket ezekre a tantárgyakra kaptak a tanáraiktól.
- Adminisztratív eszközök:
	A tanárok tudnak a diákok számára jegyeket beírni. Az adminisztrátor, aki a rendszert menedzseli, tud létrehozni osztályokat és felhasználókat (tehát a regisztráció nem nyitott a vendégek felé). Távlati terv lehet, hogy az adminisztrátor is tudja kezelni a jegyeket, stb.	

##   Kapcsolat a szerverrel
A szerver egy Rest API-n keresztül szolgálja ki az Angular alapú frontendet, ami az Angular beépített HTTP kliensének segítségével hívja meg a szükséges szerverfunkciókat.

## Egy funkció folyamatának leírása, azaz mi történik kattintástól a visszajelzésig
A választott funkció: bejelentkezés

Amikor a felhasználó megnyomja a "Bejelentkezés" gombot a bejelentkező oldalon, a rendszer ellenőrzi, hogy a megfelelő mezőket kitöltötte-e.
Ha nem, akkor nem folytatódik a bejelentkezési folyamat, hanem egy üzenet megkéri a felhasználót, hogy töltse ki a mezőket. Egyébként továbbküldi a szerver authorizációs végpontja felé a kapott adatokat. 
A szerver ezeket az adatokat összeveti az adatbázisban levő adatokkal, majd attól függően, hogy ezek helyesek-e vagy sem, visszaküld egy 200-as vagy 403-as HTTP kódot. (A 200 azt jelenti, hogy rendben, 400 pedig azt, hogy meg van tiltva a hozzáférés.)
Ha a bejelentkezési adatok helyesek, a szerver eltárolja a session storage-ban az adott munkamenetet.
A frontend, ha a szervertől 403-as kódot kap vissza, megjelenít egy hibaüzenetet, hogy a megadott adatok nem helyesek. Ilyenkor a felhasználónak újra kell próbálnia a bejelentkezési folyamatot, mivel még nem fér hozzá az alkalmazás belső oldalaihoz és funkcióihoz.
Ha a bejelentkezés sikeres, tehát a frontend 200-as kódot kap vissza, abban az esetben a kezdőoldalra irányítja a felhasználót, ami a szerepkörének megfelelő kezdőoldalt nyitja meg. Minden oldalletöltésnél újraszinkronizálja a munkamenetet a szerverrel.
Ha a bejelentkezés előtt a felhasználó megnyitott egy oldalt (de ahhoz vendégként még nem volt hozzáférésre), akkor a rendszer a bejelentkező oldalra irányítja, annak URL-ében eltárolva az adott oldal nevét. Ha ilyen van, sikeres bejelentkezés után a router arra az oldalra irányítja a felhasználót a kezdőoldal helyett.

## Tesztelés
Az alkalmazás a fejlesztés során folyamatosan tesztelve és próbálgatva lett. 
A főbb funkciók az e2e teszt segítségével lettek tesztelve, ezek bármikor futtathatók az ``ng e2e`` parancs segítségével. Ez ilyenkor elindít egy automatizált tesztet, megnyit egy Chrome böngészőt, amiben végigteszteli az adott dolgokat, mintha egy felhasználó tenné. A végén pedig összegzi, hogy a várt eredményt kapta-e.

## Felhasználói dokumentáció