# Alkalmazások fejlesztése beadandó
#### Készítette: Tóta Dávid és Zatureczki Marcell


## Projektötlet

Egy olyan webes alkalmazás, ami egy elektronikus naplót valósít meg. A tanárok tudnak osztályzatokat beírni a tantárgyaikhoz, amelyeket a diákok meg tudnak tekinteni.

### Funkcionális követelmények:
- Be- és kijelentkezés.
- Jogosultságok, szerepkörök (vendég, tanár, diák).
- A tanár tudjon osztályzatokat beírni.
- A diák át tudja tekinteni a saját osztályzatait.

### Nem funkcionális követelmények
Az alkalmazás legyen áttekinthető és felhasználóbarát.

### Szakterületi fogalomjegyzék
Nincs olyan fogalom, ami különösebb magyarázatra szorul.

### Szerepkörök
- **vendég**:
	- Nem férhet hozzá az alkalmazás belső tartalmához, szükséges számára a bejelentkezés.
- **diák**:
	- Hozzáférhet a saját adataihoz és a tantárgyaihoz, amelyeknél meg tudja tekinteni a beírt osztályzatokat.
- **tanár**:
	- A saját tantárgyaihoz osztályzatokat tud beírni a diákok számára.
- **admin**:
	- Teljes körűen tudja menedzselni a rendszert.

A további dokumentációk a backend és a frontend mappákban találhatóak!